package com.example.centroteraputicoptec

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.SmsManager
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_messenger.*
import kotlinx.android.synthetic.main.activity_messenger.ButtonSendSMS
import kotlinx.android.synthetic.main.activity_messenger.EditTextNumber
import kotlinx.android.synthetic.main.activity_messenger.EditTextMessage

class MessengerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_messenger)

        ButtonSendSMS.setOnClickListener {
            if (RadioButtonSMS.isChecked) {
                requestPermissions(arrayOf(Manifest.permission.SEND_SMS), 1)
                Log.d("TEST", "SMS")
            } else if (RadioButtonWhatsApp.isChecked) {
                Log.d("TEST", "WhatsApp")
                sendWhatsApp()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && permissions[0] == Manifest.permission.SEND_SMS && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            sendSMS()
        }
    }

    private fun sendSMS() {
        val smsManager = SmsManager.getDefault() as SmsManager
        smsManager.sendTextMessage(EditTextNumber.text.toString(), null, EditTextMessage.text.toString(), null, null)
    }

    @SuppressLint("QueryPermissionsNeeded")
    private fun sendWhatsApp() {
        // Creating intent with action send
        val intent = Intent(Intent.ACTION_SEND)

        // Setting Intent type
        intent.type = "text/plain"

        // Setting whatsapp package name
        intent.setPackage("com.whatsapp")

        // Give your message here
        intent.putExtra(Intent.EXTRA_TEXT, EditTextMessage.text.toString())

        // Checking whether whatsapp is installed or not
        if (intent.resolveActivity(packageManager) == null) {
            Toast.makeText(this,
                "Please install whatsapp first.",
                Toast.LENGTH_SHORT).show()
            return
        }

        // Starting Whatsapp
        startActivity(intent)
    }


}