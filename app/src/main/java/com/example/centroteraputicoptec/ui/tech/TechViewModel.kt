package com.example.centroteraputicoptec.ui.tech

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TechViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Capacitaciones tecnológicas Fragment"
    }
    val text: LiveData<String> = _text
}