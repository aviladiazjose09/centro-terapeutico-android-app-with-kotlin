package com.example.centroteraputicoptec.ui.about

import Mailer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.centroteraputicoptec.R
import kotlinx.android.synthetic.main.fragment_about.*

class AboutFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ButtonSendMessage.setOnClickListener {
            var message: String = EditTextMessage.text.toString()
            var error: String = Mailer.sendMail(message)

            if (error == "") {
                Toast.makeText(context, "Mensaje enviado correctamente", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(context, error, Toast.LENGTH_LONG).show()
            }

        }

    }

    /*override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }*/
}