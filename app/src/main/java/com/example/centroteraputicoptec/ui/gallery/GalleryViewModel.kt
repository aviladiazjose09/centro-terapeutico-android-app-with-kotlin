package com.example.centroteraputicoptec.ui.gallery

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class GalleryViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Asistencias Terapéuticas Fragment"
    }
    val text: LiveData<String> = _text
}