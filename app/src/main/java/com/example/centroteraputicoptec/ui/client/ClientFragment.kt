package com.example.centroteraputicoptec.ui.client

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.centroteraputicoptec.Consts
import com.example.centroteraputicoptec.R
import kotlinx.android.synthetic.main.fragment_client.*
import okhttp3.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ClientFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ClientFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    private fun getBusinessData(RUC: String) {

        val client = OkHttpClient.Builder().addInterceptor { chain ->
            val newRequest = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer ${Consts.API_TOKEN}")
                .build()
            chain.proceed(newRequest)
        }.build()

        val retrofitBuilder = Retrofit.Builder()
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Consts.BASE_URL)
            .build()
            .create(ApiInterface::class.java)

        val retrofitData = retrofitBuilder.getBusinessData(RUC)

        retrofitData.enqueue(object : Callback<ClassCompanyData?> {
            @SuppressLint("SetTextI18n")
            override fun onResponse(
                call: Call<ClassCompanyData?>,
                response: Response<ClassCompanyData?>
            ) {
                val responseBody = response.body()

                TextViewRUC.text = "RUC: ${responseBody?.numeroDocumento}"
                TextViewBusinessName.text = "Razón Social: " + responseBody?.nombre.toString()
                TextViewState.text = "Estado: ${responseBody?.estado}"
                TextViewCondition.text = "Condición: ${responseBody?.condicion}"
                TextViewDirection.text = "Dirección: ${responseBody?.direccion}"
                TextViewDistrict.text = "Distrito: ${responseBody?.distrito}"
                TextViewProvince.text = "Provincia: ${responseBody?.provincia}"
                TextViewDepartament.text = "Departamento: ${responseBody?.departamento}"
            }

            override fun onFailure(call: Call<ClassCompanyData?>, t: Throwable) {
                Log.d("TAG", "onFailure: ${t.message}")
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_client, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        RadioGroupRUCDNI.setOnCheckedChangeListener { radioGroup, optionId ->
            run {
                when (optionId) {
                    R.id.RadioButtonDNI -> {
                        TextViewRUC.visibility = TextView.GONE
                        TextViewBusinessName.visibility = TextView.GONE
                        TextViewState.visibility = TextView.GONE
                        TextViewCondition.visibility = TextView.GONE
                        TextViewDirection.visibility = TextView.GONE
                        TextViewDistrict.visibility = TextView.GONE
                        TextViewProvince.visibility = TextView.GONE
                        TextViewDepartament.visibility = TextView.GONE

                        TextViewDNI.visibility = TextView.VISIBLE
                        TextViewLastname.visibility = TextView.VISIBLE
                        TextViewMothersLastname.visibility = TextView.VISIBLE
                        TextViewNames.visibility = TextView.VISIBLE
                    }
                    R.id.RadioButtonRUC -> {
                        TextViewRUC.visibility = TextView.VISIBLE
                        TextViewBusinessName.visibility = TextView.VISIBLE
                        TextViewState.visibility = TextView.VISIBLE
                        TextViewCondition.visibility = TextView.VISIBLE
                        TextViewDirection.visibility = TextView.VISIBLE
                        TextViewDistrict.visibility = TextView.VISIBLE
                        TextViewProvince.visibility = TextView.VISIBLE
                        TextViewDepartament.visibility = TextView.VISIBLE

                        TextViewDNI.visibility = TextView.GONE
                        TextViewLastname.visibility = TextView.GONE
                        TextViewMothersLastname.visibility = TextView.GONE
                        TextViewNames.visibility = TextView.GONE
                    }
                }
            }
        }

        ButtonConsultar.setOnClickListener {
            var number: String = EditTextRUCDNI.text.toString()
            getBusinessData(number)
            getPersonData(number)
        }
    }

    private fun getPersonData(DNI: String) {
        val client = OkHttpClient.Builder().addInterceptor { chain ->
            val newRequest = chain.request().newBuilder()
                .addHeader("Authorization", "Bearer ${Consts.API_TOKEN}")
                .build()
            chain.proceed(newRequest)
        }.build()

        val retrofitBuilder = Retrofit.Builder()
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Consts.BASE_URL)
            .build()
            .create(ApiInterface::class.java)

        val retrofitData = retrofitBuilder.getPersonData(DNI)

        retrofitData.enqueue(object : Callback<ClassPersonData?> {
            @SuppressLint("SetTextI18n")
            override fun onResponse(
                call: Call<ClassPersonData?>,
                response: Response<ClassPersonData?>
            ) {
                val responseBody = response.body()

                TextViewDNI.text = "DNI: ${responseBody?.numeroDocumento}"
                TextViewLastname.text = "Apellido Paterno: ${responseBody?.apellidoPaterno}"
                TextViewMothersLastname.text = "Apellido Materno: ${responseBody?.apellidoMaterno}"
                TextViewNames.text = "Nombres: ${responseBody?.nombres}"

            }

            override fun onFailure(call: Call<ClassPersonData?>, t: Throwable) {
                Log.d("TAG", "onFailure: ${t.message}")
            }
        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ClientFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ClientFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}