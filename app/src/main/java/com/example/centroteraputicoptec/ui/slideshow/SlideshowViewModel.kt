package com.example.centroteraputicoptec.ui.slideshow

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SlideshowViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Educación sobre diversas áreas Fragment"
    }
    val text: LiveData<String> = _text
}