package com.example.centroteraputicoptec.ui.client

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiInterface {

    @Headers("Content-Type: application/json")
    @GET("ruc")
    fun getBusinessData(@Query("numero") id:String): Call<ClassCompanyData>

    @GET("dni")
    fun getPersonData(@Query("numero") DNI:String) : Call<ClassPersonData>
}