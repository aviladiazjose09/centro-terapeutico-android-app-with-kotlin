import android.util.Log
import com.example.centroteraputicoptec.service.email.config.Config
import kotlinx.coroutines.*
import org.bouncycastle.jce.provider.BouncyCastleProvider
import java.lang.Exception
import java.security.Security
import java.util.*
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage
import kotlin.system.measureTimeMillis

object Mailer {

    init {
        Security.addProvider(BouncyCastleProvider())
    }

    var error: String = ""

    private fun props(): Properties = Properties().also {
        // Smtp server
        it["mail.smtp.host"] = "authsmtp.securemail.pro"

        it["mail.smtp.auth"] = "true"

        // Port
        it["mail.smtp.port"] = "465"

        // SSL
        it["mail.smtp.ssl.enable"] = true
    }

    private fun session(emailFrom: String, emailPass: String): Session = Session.getInstance(props(), object : Authenticator() {
        override fun getPasswordAuthentication(): PasswordAuthentication {
            return PasswordAuthentication(emailFrom, emailPass)
        }
    })

    private fun builtMessage(message: String): String {
        return """
            <h1>$message<h1><br/>
        """.trimIndent()
    }

    private fun builtSubject(subject: String): String {
        return """
            $subject
        """.trimIndent()
    }

    private fun sendMessageTo(emailFrom: String, session: Session, message: String, subject: String): String {
        try {
            MimeMessage(session).let { mime ->
                mime.setFrom(InternetAddress(emailFrom))
                // Adding receiver
                mime.addRecipient(Message.RecipientType.TO, InternetAddress(Config.EMAIL_TO))
                // Adding subject
                mime.subject = subject
                // Adding message
                mime.setText(message)
                // Set Content of Message to Html if needed
                mime.setContent(message, "text/html")
                // send mail
                Transport.send(mime)
            }

        } catch (e: MessagingException) {
            //Log.d("MAIL", error)
            return e.message.toString()
            //throw Exception(e.message.toString())
        }

        return ""
    }

    fun sendMail(message: String): String {
        // Open a session
        val session = session(Config.EMAIL_FROM, Config.PASSWORD_FROM)

        // Create a message
        val message = builtMessage(message)

        // Create subject
        val subject = builtSubject("Mensaje para Admin")

        // Send Email
        //CoroutineScope(Dispatchers.IO).launch { sendMessageTo(Config.EMAIL_FROM, session, message, subject) }

        fun getErrorMessage() = CoroutineScope(Dispatchers.IO).async {
            return@async sendMessageTo(Config.EMAIL_FROM, session, message, subject)
        }
        //Thread.sleep(2000)

        //val errorMessage = getErrorMessage()

        runBlocking {
            error = getErrorMessage().await()
        }

        Log.d("EMAIL", "A: $error")

        return error
    }
}